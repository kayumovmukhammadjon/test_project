const express = require('express');
const cors = require('cors');
const config = require('./config/config');
const app = express();
const cookieParser = require('cookie-parser');
const db = require('./models/index');
const userRoutes = require('./router/userRoutes');
const fileRoutes = require('./router/fileRoutes');

const PORT = parseInt(config.server.port) || 3000;

app.use(express.json());
app.use(cors());
app.use(express.static('public'));
app.use(cookieParser());

app.use(express.urlencoded({ extended: true }));

db.sequelize.sync({ force: true }).then(() => {
  console.log('Drop and Resync Db');
});

app.use('/users', userRoutes);
app.use('/files', fileRoutes);

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
