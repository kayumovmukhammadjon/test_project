const dotenv = require('dotenv');

dotenv.config();

module.exports = {
  mysql: {
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    port: parseInt(process.env.DB_PORT) || 3306,
  },
  server: {
    port: parseInt(process.env.SERVER_PORT),
    env: process.env.NODE_ENV,
  },
};
