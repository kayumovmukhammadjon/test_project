const express = require('express');
const fileController = require('../controller/filesController');
const { uploadFile, getFiles, deleteFile, getFile, downloadFile, updateFile } =
  fileController;

const router = express.Router();

router.post('/upload', uploadFile);

router.get('/list', getFiles);

router.get('/:id', getFile);

router.delete('/delete/:id', deleteFile);

router.get('/download/:id', downloadFile);

router.put('/update/:id', updateFile);

module.exports = router;
