const express = require('express');
const userController = require('../controller/userController');
const { signUp, login } = userController;
const userAuth = require('../middleware/userAuth');
const {
  logout,
  getAccessToken,
  getInfo,
} = require('../controller/tokenController');

const router = express.Router();

router.post('/signup', userAuth.saveUser, signUp);

router.post('/signin', login);

router.get('/logout', logout);

router.get('/info', getInfo);

router.get('/signin/new_token', getAccessToken);

module.exports = router;
