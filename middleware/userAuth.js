const express = require('express');
const db = require('../models/index');

const User = db.user;

const saveUser = async (req, res, next) => {
  try {
    const phone_number = await User.findOne({
      where: {
        password: req.body.phone_number,
      },
    });
    if (phone_number) {
      return res.json(409).send('Already exists');
    }

    next();
  } catch (err) {
    console.log(err);
  }
};

module.exports = {
  saveUser,
};
