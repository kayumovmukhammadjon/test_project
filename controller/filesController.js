const db = require('../models/index');
const multer = require('multer');
const md5 = require('md5');
const path = require('path');

const Files = db.files;

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve('public', 'uploads'));
  },
  filename: function (req, file, cb) {
    cb(null, `${md5(Date.now())}${path.extname(file.originalname)}`);
  },
});

const upload = multer({ storage: storage }).single('files');

const uploadFile = async (req, res) => {
  try {
    upload(req, res, async (err) => {
      if (err) {
        return res.status(400).send(err);
      }
      const { originalname, mimetype, size, filename } = req.file;
      const file = await Files.create({
        name: originalname,
        path: filename,
        mimetype: mimetype,
        size: size,
        extension: filename.split('.')[1],
      });
      return res.status(201).send(file);
    });
  } catch (err) {
    console.log(err);
  }
};

const getFiles = async (req, res) => {
  try {
    const { page, list_size } = req.query;
    const files = await Files.findAndCountAll({
      limit: list_size || 10,
      offset: (page - 1) * list_size || 0,
    });
    return res.status(200).send(files);
  } catch (err) {
    console.log(err);
  }
};

// delete files from the database and local storage
const deleteFile = async (req, res) => {
  try {
    const { id } = req.params;
    const file = await Files.findOne({ where: { id } });
    if (file) {
      await Files.destroy({ where: { id } });
      return res.status(200).send('File deleted');
    } else {
      return res.status(400).send('File not found');
    }
  } catch (err) {
    console.log(err);
  }
};

const downloadFile = async (req, res) => {
  try {
    const { id } = req.params;
    const file = await Files.findOne({ where: { id } });
    if (file) {
      return res.download(`public/uploads/${file.path}`);
    } else {
      return res.status(400).send('File not found');
    }
  } catch (err) {
    console.log(err);
  }
};

const updateFile = async (req, res) => {
  try {
    const { id } = req.params;
    const file = await Files.findOne({ where: { id } });
    if (file) {
      upload(req, res, async (err) => {
        if (err) {
          return res.status(400).send(err);
        }
        const { originalname, mimetype, size, filename } = req.file;
        await Files.update(
          {
            name: originalname,
            path: filename,
            mimetype: mimetype,
            size: size,
            extension: filename.split('.')[1],
          },
          { where: { id } }
        );
        return res.status(200).send('File updated');
      });
    } else {
      return res.status(400).send('File not found');
    }
  } catch (err) {
    console.log(err);
  }
};

const getFile = async (req, res) => {
  try {
    const { id } = req.params;
    const file = await Files.findOne({ where: { id } });
    console.log(file);
    if (file) {
      return res.status(200).send(file);
    } else {
      return res.status(400).send('File not found');
    }
  } catch (err) {
    console.log(err);
  }
};

module.exports = {
  uploadFile,
  getFiles,
  deleteFile,
  downloadFile,
  getFile,
  updateFile,
};
