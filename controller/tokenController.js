const db = require('../models/index');
const jwt = require('jsonwebtoken');
const { refreshTokenBodyValidation } = require('../utils/validationSchema');
const verifyRefreshToken = require('../utils/verifyRefreshToken');
const Token = db.token;
const User = db.user;

const getInfo = async (req, res) => {
  try {
    const token = req.header('Authorization').split(' ')[1];

    const tokenData = await Token.findOne({ where: { access_token: token } });

    if (!tokenData)
      return res.status(401).json({ error: true, message: 'Invalid token' });

    const user = await User.findOne({ where: { id: tokenData.userId } });
    if (!user)
      return res.status(401).json({ error: true, message: 'Invalid token' });

    res.status(200).json({
      error: false,
      message: 'User info fetched successfully',
      user: user,
    });
  } catch (err) {
    res.status(400).json({ error: true, message: 'Invalid token' });
  }
};

//logout
const logout = async (req, res) => {
  try {
    const token = req.header('Authorization').split(' ')[1];
    const { error } = refreshTokenBodyValidation({ refreshToken: token });
    if (error) {
      return res.status(400).json({
        error: true,
        message: error.details[0].message,
      });
    }
    const userToken = await Token.findOne({ token: req.body.refreshToken });
    if (!userToken)
      return res
        .status(200)
        .json({ error: false, message: 'Logged Out Sucessfully' });

    await userToken.remove();
    res.status(200).json({ error: false, message: 'Logged Out Sucessfully' });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: true, message: 'Internal Server Error' });
  }
};

const getAccessToken = async (req, res) => {
  try {
    const token = req.header('Authorization').split(' ')[1];
    const { error } = refreshTokenBodyValidation({ refreshToken: token });
    if (error) {
      return res.status(400).json({
        error: true,
        message: error.details[0].message,
      });
    }
    const userToken = await Token.findOne({ token: req.body.refreshToken });
    console.log(userToken);
    if (!userToken)
      return res
        .status(200)
        .json({ error: false, message: 'Logged Out Sucessfully' });

    const payload = { _id: userToken.userId };
    const accessToken = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, {
      expiresIn: '10m',
    });
    res.status(200).json({
      error: false,
      accessToken,
      message: 'Access token generated successfully',
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: true, message: 'Internal Server Error' });
  }
};

module.exports = {
  getAccessToken,
  logout,
  getInfo,
};
