const Joi = require('joi');
const passwordComplexity = require('joi-password-complexity');

const signUpBodyValidation = (body) => {
  console.log(body);
  const schema = Joi.object({
    phone_number: Joi.string()
      .required()
      .label('Phone number'),
    password: passwordComplexity()
      .required()
      .label('Password'),
  });
  return schema.validate(body);
};

const logInBodyValidation = (body) => {
  const schema = Joi.object({
    phone_number: Joi.string()
      .required()
      .label('Phone number'),
    password: passwordComplexity()
      .required()
      .label('Password'),
  });
  return schema.validate(body);
};

const refreshTokenBodyValidation = (body) => {
  const schema = Joi.object({
    refreshToken: Joi.string()
      .required()
      .label('Refresh Token'),
  });
  return schema.validate(body);
};

module.exports = {
  signUpBodyValidation,
  logInBodyValidation,
  refreshTokenBodyValidation,
};
