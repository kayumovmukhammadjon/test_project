const jwt = require('jsonwebtoken');
const db = require('../models/index');

const Token = db.token;

const generateTokens = async (user) => {
  try {
    const payload = { _id: user.id };
    const accessToken = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, {
      expiresIn: '10m',
    });

    const refreshToken = jwt.sign(payload, process.env.REFRESH_TOKEN_SECRET, {
      expiresIn: '10m',
    });

    const token = await Token.findOne({ where: { userId: user.id } });
    if (token) await token.destroy();

    await Token.create({ access_token: refreshToken, userId: user.id });

    return Promise.resolve({ accessToken, refreshToken });
  } catch (err) {
    return Promise.reject(err);
  }
};

module.exports = generateTokens;
